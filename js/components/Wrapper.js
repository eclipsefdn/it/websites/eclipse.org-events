import React, { useState } from 'react';
import CustomSearch from './CustomSearch';
import EventsCheckboxFilters from './pastAndUpcomingEvents/EventsCheckboxFilters';
import EventsDataFetcher from './pastAndUpcomingEvents/EventsDataFetcher';
import { EVENT_ATTENDANCE_TYPE, EVENT_PARTICIPATION_TYPE, EVENT_TYPES, WORKING_GROUPS } from './EventHelpers';

const Wrapper = () => {
  const [triggerSearchValue, setTriggerSearchValue] = useState('');
  const [checkedWorkingGroups, setCheckedWorkingGroups] = useState({});
  const [checkedTypes, setCheckedTypes] = useState({});
  const [checkedAttendance, setCheckedAttendance] = useState({});
  const [checkedParticipation, setCheckedParticipation] = useState({});
  const [upcomingReachEnd, setUpcomingReachEnd] = useState(false);

  return (
    <>
      <div className="container">
        <div className="row margin-bottom-20">
          <div className="col-md-6 margin-top-20">
            <a className="btn btn-primary" href="https://newsroom.eclipse.org/node/add/events">
              Submit Your Event
            </a>
            <CustomSearch triggerSearchValue={triggerSearchValue} setTriggerSearchValue={setTriggerSearchValue} />
            <EventsCheckboxFilters
              checkedBoxes={checkedTypes}
              setCheckedBoxes={setCheckedTypes}
              filterOptions={EVENT_TYPES}
              filterGroupTitle="EVENT TYPE"
            />
            <EventsCheckboxFilters
              checkedBoxes={checkedAttendance}
              setCheckedBoxes={setCheckedAttendance}
              filterOptions={EVENT_ATTENDANCE_TYPE}
              filterGroupTitle="ATTENDANCE"
            />
            <EventsCheckboxFilters
              checkedBoxes={checkedParticipation}
              setCheckedBoxes={setCheckedParticipation}
              filterOptions={EVENT_PARTICIPATION_TYPE}
              filterGroupTitle="PARTICIPATION"
            />
            <EventsCheckboxFilters
              checkedBoxes={checkedWorkingGroups}
              setCheckedBoxes={setCheckedWorkingGroups}
              filterOptions={WORKING_GROUPS}
              filterGroupTitle="CATEGORIES"
            />
          </div>

          <div className="col-md-18 event-list-wrapper">
            <EventsDataFetcher
              eventTime="upcoming"
              searchValue={triggerSearchValue}
              checkedWorkingGroups={checkedWorkingGroups}
              checkedTypes={checkedTypes}
              checkedAttendance={checkedAttendance}
              checkedParticipation={checkedParticipation}
              reachEnd={upcomingReachEnd}
              setReachEnd={setUpcomingReachEnd}
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default Wrapper;
