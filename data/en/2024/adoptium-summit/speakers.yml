items:
  - name: "Bruno Verachten"
    title: "Senior Developer Relations, Cloudbees"
    img: "bruno-verachten.jpg"
    bio: |
      <p>
        👨‍💻 Dad of two, husband of one, I've been secretly geeking out
        since ‘89 &mdash; back when handheld devices were as cool as shoulder
        pads.
      </p>
      <p>
        🐝 I dabble in beekeeping and permaculture, and yes, I'm the
        Linux-loving, Android-tinkering, Docker-dabbling, ARMV8 aficionado in
        the room.
      </p>
      <p>
        🌱 When I’m not convincing my gadgets to get along, you'll find me
        knee-deep in the world of Linux and open source, treating single-board
        computers like the rockstars they are.
      </p>
      <p>
        🌿 Beyond tech, I'm into the zen of gardening, the art of handcrafted
        woodworking, and the thrill of bike riding.
      </p>
      <p>
        🗣️ Oh, and did I mention I’'m a bit of a chatterbox, even though I might
        seem low-key?
      </p>
      <p>
        💻 On a mission to spread the gospel of ARM and RISC-V
        Socs&mdash;because who needs a boring CPU when you can have a tech
        rockstar?
      </p>
  - name: "Carmen Delgado"
    title: "Community Manager, Eclipse Foundation"
    img: "carmen-delgado.jpg"
    bio: |
      <p>
        As the Community Manager for Eclipse Adoptium since 2022, Carmen brings a wealth of experience in project, 
        operations, and financial management across various industries to help Adoptium working group members 
        achieve their goals and objectives. Her background includes successful terms in healthcare, pharma, fintech, 
        and tech startups. Additionally, She actively contributes to Step4ward, a mentoring program in Spain, 
        demonstrating her commitment to fostering diversity and inclusion in the tech world.
      </p>
  - name: "Daniel Scanteianu"
    title: "Software Engineer, Bloomberg"
    img: "daniel-scanteianu.jpg"
    bio: |
      <p>
        Daniel Scanteianu is a software engineer at Bloomberg in London and an
        Open Source enthusiast. As a contributor to the Eclipse Foundation’s
        Adoptium project, he is a member of the Adoptium Secure Development
        Working Group, where he works on the Adoptium Vulnerability Disclosure
        Report. He has also contributed to Apache Kafka. In his day job, he is
        one of the leaders of Bloomberg’s Java and JVM Guild. His interests
        include distributed systems, domain specific languages, and big data.
      </p>
  - name: "Haroon Khel"
    title: "Software Engineer, Red Hat"
    img: "haroon-khel.jpg"
    bio: |
      <p>
        My name is Haroon Khel. I am a software engineer at Red Hat. I am a
        member of the infrastructure team at Adoptium.
      </p>
  - name: "Lan Xia"
    title: "Quality Automation Architect, IBM Runtime Technologies"
    img: "lan-xia.jpg"
    bio: |
      <p>
        Lan Xia is the Quality Automation Architect at IBM Runtime
        Technologies. She is an active committer and contributor for the open
        source projects – Eclipse OpenJ9 and Eclipse AQAvit, where she also
        serves on Adoptium Steering Committee and Adoptium Project Management
        Committee member. She champions the creation of transformative
        solutions such as the Test Results Summary Service and AQA Test
        Pipelines, integrating AI to enhance and expedite the current
        development practices.
      </p>
  - name: "Longyu Zhang"
    title: "Software Developer, IBM Runtime Technologies"
    img: "longyu-zhang.jpg"
    bio: |
      <p>
         Longyu Zhang, Ph.D, is a software developer at IBM Runtime
         Technologies. He is a committer of Eclipse AQAvit open-source project
         and an active contributor of Eclipse Adoptium and Eclipse Openj9. He
         also works closely with industrial and academic researchers to develop
         innovative machine learning solutions for various applications.
      </p>
  - name: "Michael Winser"
    title: "Co-Founder and Technical Strategist, Alpha-Omega"
    img: "michael-winser.jpg"
    bio: |
      <p>
        Michael is a 40 year veteran in the software industry, with over 25 of
        those years at Google and Microsoft. Michael is also a Security
        Strategy Ambassador for the Eclipse Foundation. and is the co-founder
        of and technical strategist for Alpha-Omega. Michael is an industry
        expert in software supply chain security, software development, and
        developer ecosystems. Michael also works with corporations and open
        source organizations to develop and execute on their security strategy.
      </p>
  - name: "Sanhong Li"
    title: "Java Champion and Co-Leader of GreenTEA JUG"
    img: "sanhong-li.jpg"
    bio: |
      <p>
        Sanhong Li has been working on Java since 2004, where he began at Intel
        R&D Lab, implementing JSR135. He progressed to working on developing
        IBM’s J9VM in 2010, where he led a project to develop multi-tenancy
        technology for the JVM. In 2014, he joined Alibaba to lead the
        development of Alibaba Dragonwell, a downstream of OpenJDK. He has
        authored over twenty technical patents/papers in the areas of managed
        runtime and compiler and presented at various conferences. He is the
        co-leader of the GreenTea JUG (the largest JUG in China), Alibaba’s
        representative of JCP Executive Committee, GraalVM Project Advisory
        Board, and Eclipse Adoptium Working Group. In 2020, Sanhong Li became a
        Java Champion for services rendered to the Java ecosystem.
      </p>
  - name: "Scott Fryer"
    title: "Senior Software Engineer, Red Hat"
    img: "scott-fryer.jpg"
    bio: |
      <p>
        Although a recent hire of Red Hat, I have extensive experience (10 years) 
        within the utility industry, particularly concerning implementing software 
        packages that met the deregulation and competition requirements to operate 
        within the new markets. In addition, I have had a 16-year career within the UK 
        financial services software industry, largely responsible for implementing 
        software compliance standards around source code auditing and control and ensuring
        the auditing of software releases to ensure traceability and consistency within components.
      </p>
  - name: "Shelley Lambert"
    title: "Senior Principal Software Engineer, Red Hat"
    img: "shelley-lambert.jpg"
    bio: |
      <p>
        Shelley is a Senior Principal Software Engineer at Red Hat. She is a
        PMC member at Eclipse Adoptium and serves as project lead and committer
        on several Eclipse projects. She is deeply committed to creating
        environments where everyone has opportunities to learn and grow. A
        great deal of her time is spent ensuring the projects she is
        responsible for are flourishing, which includes actively engaging new
        and first-time open-source contributors.
      </p>
