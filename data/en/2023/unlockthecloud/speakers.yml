items:
  - name: Astor Nummelin Carlberg
    title: Executive Director, OpenForum Europe
    img: astor-nummelin-carlberg.jpg
    bio: |
      <p>
        Astor Nummelin Carlberg is OFE's Executive Director, responsible for the overall vision, 
        activities of the organization and policy development. He has extensive experience of 
        European policy making processes, communications and network-building. Astor leads 
        conversations on Europe's digital challenges and the role of open technologies in achieving 
        its full potential. He sits on the board of APELL, the European Open Source Business 
        Association. Prior to OFE, Astor worked in the European Parliament and he was educated at 
        Middlebury College, the Free University of Berlin and Solvay Business School.
      </p>
  - name: Clark Parsons
    title: Managing Director, Internet Economy Foundation
    img: clark-parsons.jpg
    bio: |
      <p>
        Heads the Internet Economy Foundation as Managing Director. Formerly working as a 
        journalist, business consultant and country manager at an international IT-sales network, 
        the American has lived in Berlin since 1998. He co-founded VoIP telephony company Vortel 
        GmbH and was long-standing Managing Director at the Berlin School of Creative Leadership.
      </p>
  - name: Kurt Garloff
    title: CTO Sovereign Cloud Stack (SCS), OSB Alliance e.V.
    img: garloff.jpg
    bio: |
      <p>
        Kurt Garloff has spent his professional life with Open Source projects. Educated as physicist 
        (in Dortmund and Eindhoven), he has a desire to fundamentally understand things. Applying 
        this to computers made chosing Open Source a natural consequence. In the end, the global 
        collaboration with smart people and the contribution to democratizing a technology that's 
        becoming more and more important became the most fascinating drivers in contributing to open 
        source. He went from being a Linux kernel developer to someone who loves working intensely 
        with people that do such work. From time to time, he still loves touching code himself.
      </p>
      <p>
        Leading the growth of SUSE Labs, he had the opportunity to forge successful teams from 
        brilliant individuals. He later complemented this with technical, strategic and business 
        leadership functions in SUSE and received training at the Harvard Business School to hone 
        the needed skills. Since 2011, his focus is on cloud technology and is an active particpant 
        of the Open Infrastructure Community. As VP Engineering at Deutsche Telekom and later as 
        chief architect of the Open Telekom Cloud, he has had leadership roles in building quite 
        some open source cloud infrastructure.
      </p>
      <p>
        Since early 2020, he is working with an increasing number of co-workers on defining and 
        assembling a standardized, open, modular technology stack that enables a large number of 
        operators to jointly provide large, federated, open cloud & container infrastructure. This 
        is the Sovereign Cloud Stack (SCS) project of the Open Source Business Alliance. It 
        particpates actively in Gaia-X and has received a significant grant from the German 
        ministry for economic affairs, has already released R4 of the reference implementation in 
        March 2023. Four public cloud providers are in production with it as of April 2023, more 
        are upcoming.
      </p>
  - name: Michael Plagge
    title: Vice President Ecosystem Development, Eclipse Foundation
    img: michael-plagge.jpg
    bio: |
      <p>
        Michael Plagge joined the Eclipse Foundation in 2021 and is now working as VP Ecosystem Development.
      </p>
      <p>
        In a previous life he started his career as software developer in the area of automotive software before he changed to the dark side of business development.
      </p>
      <p>
        Here he held various business development positions in companies like Elektrobit, Alibaba and Alibaba Cloud and spent 3 years as General Manager Elektrobit Automotive (Shanghai) Ltd. in China.
      </p>
  - name: Markus Spiekermann 
    title: Head of Department "Data Business", Fraunhofer Institute for Software and Systems Engineering
    img: markus-spiekermann.jpg
    bio: |
      <p>
        Markus Spiekermann is Head of Department "Data Business" at the Fraunhofer Institute for 
        Software and Systems Engineering in Dortmund, Germany. His main research focuses on the 
        economic and engineering perspectives of data spaces. Since 2016, he is involved in various 
        dataspace initiatives like IDSA, Gaia-X, the DSSC and a large number of domain driven 
        initiatives that aim on building and running dataspaces. He also leads the Eclipse 
        Dataspace Component (EDC) project, which provides an open-source framework and flexible 
        architecture for service providers to implements their dataspace components and commercial 
        offerings on a production-ready basis.
      </p>
      <p>
        The Fraunhofer Institute for Software and Systems Engineering ISST is one of the 74 
        research institutes of the Fraunhofer Gesellschaft, located in Dortmund (Germany). Founded 
        in 1992, Fraunhofer ISST has focused on software development and system technologies with 
        special expertise in the field of dataspace technologies and corresponding business models.
      </p>
  - name: Stephan Ilaender
    title: General Manager Service & Runtimes, STACKIT
    img: stephan-ilaender.jpg
    bio: |
      <p>
        Since early 2020, he is working with an increasing number of co-workers on defining and 
        assembling a standardized, open, modular technology stack that enables a large number of 
        operators to jointly provide large, federated, open cloud & container infrastructure. This 
        is the Sovereign Cloud Stack (SCS) project of the Open Source Business Alliance. It 
        particpates actively in Gaia-X and has received a significant grant from the German 
        ministry for economic affairs, has already released R4 of the reference implementation in 
        March 2023. Four public cloud providers are in production with it as of April 2023, more 
        are upcoming.
      </p>
  - name: Gael Blondelle
    title: Chief Membership Officer, Eclipse Foundation
    img: gael-blondelle.jpg
    bio: |
      <p>
        Gaël Blondelle joined the Eclipse Foundation in 2013 and now serves as Chief Membership Officer. He has been involved in the open source arena for more than 18 years in a number of key roles.
      </p>
      <p>
        Gaël co-founded an open source start-up and worked as its Chief Technology Officer. Gaël then worked in business development for an open source systems integration company and managed highly strategic research IT projects aiming to create open source ecosystems for major industrial players. Gaël joined the Eclipse Foundation to pursue his goal of helping more companies work in open source, and to grow open, innovative and collaborative ecosystems for mission-critical applications.
      </p>
  - name: Mark Reeve 
    title: Cloud Research Director, DECISION Etudes & Conseil 
    img: mark-reeve.jpg
    bio: |
      <p>
        Mark Reeve has been working on technology and infrastructure for the past 18 years. After deploying Datacenters and Data Rooms in over 89 countries, he has been working on defining the strategy, adoption path and reference architecture patterns for hybrid and multi cloud deployments, leveraging both open and proprietary technologies.
      </p>
  - name: Thomas Weber
    title: Chief Product Officer for the West European Region, Huawei Technologies
    img: thomas-weber.jpg
    bio: |
      <p>
        Thomas Weber is Chief Product Owner for the West European Region at
        Huawei Technologies. He is responsible for planning new services for the
        Public Cloud platforms of Huawei, incorporating customer requirements,
        and coordinating with the R&D side of Huawei to provide these new
        services. He also provides technical support for large companies, e.g. to
        integrate existing systems, infrastructure and business processes and to
        enable the introduction of public, private and hybrid cloud
        architectures. Thomas is addressing many technologic fields at Huawei
        such as Big Data and AI, Quantum Computing and high performance
        computing.
      </p>
  - name: Emma Wehrwein
    title: Project Lead, GXFS-DE
    img: emma-wehrwein.jpg
    bio: |
      <p>
        Emma Wehrwein is Project Manager Digital Business Models at eco - 
        Association for the Internet Industry. She holds a master's degree of
        Science and worked as IT Project Manager within the chemical industry for
        many years before joining eco. Since 2020 she is heading the Project
        Management Office for the German Gaia-X Federation Services initiative
        and is active as Ambassador for Gaia-X via Events and Social Media.
      </p>
  - name: Alban Schmutz
    title: Independent Board of CISPE
    img: alban-schmutz.jpg
    bio: |
      <p>
        Alban Schmutz is currently Independent Board of CISPE (Cloud Infrastructure
        Services Providers in Europe), and was one of its co-founder and Chairman
        (2016-2022).
      </p>
      <p>
        Within CISPE, Alban actively participated to deliver the first Cloud
        infrastructure GDPR Code of Conduct validated by the European Data Protection
        Board (CISPE Data Protection Code of Conduct), the 10 Principles for Fair
        Software Licensing for Cloud users developed with CIO associations or the SWIPO
        Code for Portability of infrastructure in the Cloud. In addition, Alban has
        been the co-founder of the Climate Neutral Data Centre Pact, which engaged the
        Cloud and Data Centre industry towards climate neutrality by 2030 in
        collaboration with the European Commission.
      </p>
      <p>
        Alban has been one of the co-founders of Gaia-X, former Member of the Board and
        co-Chair Policy Rules Committee.
      </p>
      <p>
        He served for 10 years as Senior Vice-President, Business Dev. & Public Affairs
        at OVHcloud (2012-2022).
      </p>
      <p>
        Alban started several companies in the Open Sources services and High
        Performance Computing, and served in various Boards of research institutes,
        engineering schools or competitiveness clusters. He hold Master’s degrees in
        Telecommuncation, Political Sciences and Entrepreneurship.
      </p>
