---
title: The Eclipse Languages Symposium
date: 2005-10-26
summary: The goal of the Languages Symposium was to get a group of smart engineers and researchers together for two days to talk about, and plan the future of, language support in Eclipse.
hide_sidebar: true
keywords: ["The Eclipse Languages Symposium"]
layout: single
---

## Position Papers Received So Far;

- [Martin Aeschlimann &quot;A Seed for an Eclipse Language Toolkit&quot;](./a-seed-for-an-eclipse-language-toolkit)
- [David Ascher &quot;An Alien's Perspective&quot;](./ascher-eclipse-position.pdf)
- [Ward Cunningham &quot;TestPoint In Practice&quot;](./test-point-in-practice.pdf)
- [Julian Dolby &quot;Using Static Analysis For IDE's for Dynamic Languages&quot;](./julian-dolby-paper.pdf)
- [John Duimovich &quot;Common Eclipse support for language IDE&quot;](common-eclipse-support-for-language-ides)
- [Brian Foote &quot;Architectural Balkanization in the Post-Linguistic Era&quot;](./balkanization.pdf)
- [Bjorn Freeman-Benson &quot;Extending Eclipse with Languages Other Than Java&quot;](./extending-eclipse-with-languages-other-than-java.pdf)
- [Leif Frenzel &quot;Extending Eclipse in Haskell&quot;](./extending-eclipse-in-haskell.pdf)
- [Doug Gaff &quot;Extending Eclipse with Compiled Languages Other than C/C++&quot;](./extending-eclipse-with-compiled-languages-other-than-c.pdf)
- [Guy Harpaz &quot;Multi Language Support (MLS) - User perspective&quot;](./multi-language-support-user-perspective.pdf)
- [Martin Imrisek &quot;Compiled Language Debug as Extensions to Platform Debug vs. Parallel Hierarchy&quot;](./m-imrisek-languages-workshop-position-paper.pdf)
- [Mike Milinkovich &quot;Staying Pragmatic&quot;](./staying-pragmatic.pdf)
- [Jeffrey Overbey &quot;Instant IDEs: Supporting New Languages in the CDT&quot;](./etx-paper.pdf)
- [Andrey Platov &quot;Eclipse and Dynamic Languages&quot;](./eclipse-and-dynamic-languages.pdf) (not attending)
- [Alan Pratt &quot;A note for the Eclipse Language Conference&quot;](./a-note-for-the-eclipse-language-conference.pdf) (not attending)
- [Craig Rasmussen &quot;Native Parser/Compiler Front Ends for Eclipse?&quot;](./craig-rasmussen.pdf)
- [Chris Recoskie &quot;&quot;Compiled&quot; Language Support in Eclipse&quot;](./compiled-language-support-in-eclipse.pdf)
- [Reginald Reed &quot;Eclipse Everywhere&quot;](./eclipse-everywhere.pdf)
- [Doug Schaefer &quot;Can the CDT DOM be used to represent languages other than C and C++&quot;](./doug-lang-symp.pdf)
- [Michael Scharf &quot;Split along Technology not Language&quot;](./split-along-technology-not-language-2005-10.pdf)
- [Markus Schorn &quot;Adding support for a new language to an Eclipse based IDE&quot;](./adding-support-for-a-new-language-to-an-eclipse-based-ide.pdf)
- [Mikhail Sennikovsky &quot;CDT Managed Build System&quot;](./cdt-managed-build-system.pdf)
- [Leo Treggiari &quot;Build Configurations &amp; Tool-chains&quot;](./lang-symposium-oct-2005.pdf)
- [Elias Volanakis &quot;Mixed-language usage scenarios in Eclipse&quot;](./volanakis-mixed-language-usage-scenarios.pdf)
- [Ed Warnicke &quot;A Desire for a Point of Integration&quot;](./eclipse-language-position.pdf)
- [David Williams &quot;Language Support in Eclipse WTP/SSE&quot;](language-support-in-eclipse-wtp-sse)

[All papers in one zip.](all.zip)

## Workshop Format

We'll run the workshop in the [LAWST](http://www.kaner.com/lawst.htm) / [WOPR](http://www.performance-workshop.org/content/view/28/64/) style. The workshop will have seven sessions, each with a different topic emphasis:

*   Thursday 9-10:30 - Overview/Architecture
*   Thursday 11:00-12:30 - Existing Frameworks
*   _lunch_
*   Thursday 1:30-3:00 - IDE for Dynamic Languages
*   Thursday 3:30-5:00 - _wild card_
*   _dinner_
*   Friday 9:00-10:30 - IDE for Compiled Languages
*   Friday 11:00-12:30 - Plug-ins Written in Other Languages
*   _lunch_
*   Friday 1:30-3:00 - _wild card_

For each session, I will choose one of the relevant attendees to be the center of the discussion. We'll start with a question like "tell us about what you did" and the center will give us an unrehearsed, but very knowledgeable description, probably based on their position paper. Then we'll spend about half an hour discussing the center's story; questions like "how did that work?" and "how did you solve this problem?" are legitimate - questions that form little mini-lectures about "I did X" are not. Once we have extracted the center's story and commingled it with the experiences of the others, we will open the discussion up to others with similar systems, experiences, ambitions or beliefs. Here's where you can tell a bit about your story while still staying focus on the session's topic.

The wild card sessions at the end of each day will be used to continue or extend interesting topics that arise during the day. They might be used for another center of discussion session or they might be used to brainstorm about direction and action items or they might be used to continue an earlier discussion or ...?

## Older Paragraphs about the Workshop Format

We'll run the workshop in the [LAWST](http://www.kaner.com/lawst.htm) / [WOPR](http://www.performance-workshop.org/content/view/28/64/) style. This means that all attendees must submit a position paper. No paper; no attendance; no exceptions (even Mike and I will write position papers). The position paper should be on one of the four topics:

*   Eclipse Language Support for dynamically typed languages (Perl, Tcl, Python, PHP, Ruby,etc).
*   Support for writing Eclipse plug-ins in dynamically typed languages (Python, Ruby, Tcl, etc).
*   Generic compiled language debugging infrastructure and implementation
*   Current state and future of the text and document model in Eclipse
*   All issues related to supporting compiled and dynamic languages in Eclipse

The intent of requiring a position paper is to (1) have everyone better prepare by writing — or at least outlining — a paper and reading all the other papers prior to a session, and (2) frankly to filter out the idly curious. These people are not unwelcome, but especially when they dilute the discussion or occupy limited seats then their attendance does not have high priority. The idea is to have brief position papers or longer, more developed papers to help each of us lead a group-wide discussion. In the most recent LAWST workshop session, several papers were only one or two pages long, and the longest was about 25 pages. The flavor is different than traditional conferences, with mostly one-way communication to a large group. These workshop sessions are more like small group peer reviews with lots of impromptu brainstorming.

The position papers are all distributed (email) in advance. The papers will be grouped into approximately six sessions. Not all papers will be included (there might just not be a logical place). Each session will start with presentations by the select authors. Not slick powerpoint presentations, but more of an interactive talk: the author will describe his or her experiences, ideas, difficulties, and so on. A couple powerpoint slides or, better yet, a demo, are acceptable, but the key here is that this is a workshop - an interactive conversation - not a lecture. In general, the talk should be about successes and future successes, but instructive failures are also welcome. After the war stories, there is general discussion between the audience and the speakers - the key feature of the discussion is that it stays focused on the speakers' points and does not devolve into each audience member giving a short speech about his or her own ideas.

## Logistics

*   The symposium will be held October 27th and 28th at the QNX headquarters at [175 Terence Matthews Crescent, Kanata, ON, Canada](http://maps.google.com/maps?q=175+Terence+Matthews+Cres,+Kanata,+ON,+Canada&spn=0.003736,0.005657&t=k&hl=en). There is no registration fee for this workshop which also means that you're responsible for your own transportation and accommodation. Sharon (the Eclipse administrator and Ottawa resident) suggests these hotels:
    *   [Holiday Inn Select](http://www.ichotelsgroup.com/h/d/hi/1/en/hd/yowow?irs=null)    Middle of the road
    *   [Brookstreet](http://www.brookstreethotel.com/reach_us/)        High end
    *   [Days Inn](http://www.daysinnottawawest.com/cont.htm)         BasicThey are all about 10-15 minutes by car from QNX.
*   We will begin each day at 8:30am 9:00am. Not everyone can attend both days (some people are in the CDT workshop that overlaps by a day; some people have early flights out on Friday), but we'll do the best we can. We will have a group dinner outing Thursday evening.
*   The CDT Workshop that we are overlapping with is holding a party Wednesday night, October 26th and we're all invited. [Linda Campbell](mailto:linda@qnx.com) of QNX would like your RSVP if you plan to attend so that she can order enough food. The details are:
    *   Pizza, Beer, Pool @ Le Skratch - [1642 Merivale Road, Nepean, Ontario](http://maps.google.com/maps?q=1642+Merivale+Road,+Nepean,+Ontario&t=k&iwloc=A&hl=en) - 6:30 p.m. until 9:00 p.m or later.

### The CDT Workshop

Note that QNX is hosting the CDT workshop Tue-Wed-Thurs at the same location. In fact, it is QNX's generosity with their space that allows this Languages Workshop to occur. The key thing to remember is that the CDT Workshop (Tue-Wed-Thurs) is separate from the Languages Workshop (Thurs-Fri). They are co-located to take advantage of many of your travel plans, but they are separate events. I'm not organizing the CDT Workshop; I am organizing the Languages Workshop. 

## Goal of the Languages Symposium

The goal is to get a group of smart engineers and researchers together for two days to talk about, and plan the future of, language support in Eclipse. The symposium will be a very interactive workshop - not a conference with formal papers and lots of bored listeners reading email on their laptops.

## Deadlines

The original deadline was Friday, October 14th, but we all know that these things slip a bit, so the new deadline is a week later: Friday, October 21st. But I'm still happy to accept very very late papers - so send them in.

To attend, you must submit a position paper.