---
title: "EclipseCon 2022"
type: "eclipsecon"
layout: "section"
hide_footer_logo: true
description: "Conference for the Eclipse Ecosystem | Ludwigsburg, Germany | October 24 - 27, 2022"
summary: |
  EclipseCon is the leading conference for developers, architects, and open
  source business leaders to learn about Eclipse technologies, share best
  practices, and more. EclipseCon is our biggest event of the year and connects
  the Eclipse ecosystem and the industry’s leading minds to explore common
  challenges and innovate together on open source runtimes, tools, and
  frameworks for cloud and edge applications, IoT, artificial intelligence,
  connected vehicles and transportation, digital ledger technologies, and much
  more.
---

{{< eclipsecon/2022 >}} 


