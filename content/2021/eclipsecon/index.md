---
title: "EclipseCon 2021"
headline: "EclipseCon 2021"
subtitle: "Thank you for another great year. Session recordings are now available on YouTube."
tagline: "Save the dates for <br> EclipseCon 2022 | October 24 - 27, 2022 | Ludwigsburg, Germany"
jumbotron_tagline_class: "col-xs-24"
hide_page_title: true
hide_sidebar: true
hide_footer_logo: true
description: "Conference for the Eclipse Ecosystem | October 25 - 28, 2021 | Ludwigsburg, Germany"
summary: |
  EclipseCon is the leading conference for developers, architects, and open
  source business leaders to learn about Eclipse technologies, share best
  practices, and more. EclipseCon is our biggest event of the year and connects
  the Eclipse ecosystem and the industry’s leading minds to explore common
  challenges and innovate together on open source runtimes, tools, and
  frameworks for cloud and edge applications, IoT, artificial intelligence,
  connected vehicles and transportation, digital ledger technologies, and much
  more.
---

{{< eclipsecon/2021 >}}
