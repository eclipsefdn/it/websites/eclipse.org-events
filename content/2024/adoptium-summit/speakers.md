---
title: "Speakers"
date: 2024-07-09 
description: "The speakers featured at Adoptium Summit 2024."
categories: []
keywords: []
slug: ""
aliases: []
toc: false
hide_page_title: false
hide_sidebar: true
hide_breadcrumb: false
layout: "speakers"
---

{{< events/user_bios event="adoptium-summit" year="2024" source="speakers" imgRoot="../images/speakers/" >}}
