---
title: "Adoptium Summit"
date: 2024-12-04
description: "Adoptium Summit is a half day premier virtual conference."
keywords: ["Adoptium", "Summit", "Event", "Virtual", "Java", "Temurin User", "AQAvit", "Eclipse Temurin", "Java Migration", "SecureDev"]
headline: ""
custom_jumbotron: |
  <div class="custom-jumbotron">
    <div class="custom-jumbotron-main">
      <h1 class="custom-jumbotron-title">Adoptium Summit 2024</h1>
      <p class="custom-jumbotron-subtitle">Migrating Towards Securely Developed Open Source Solutions</p>
      <p class="custom-jumbotron-tagline">Virtual Conference | 10 September 2024</p>
    </div>
    <aside class="custom-jumbotron-showcase study-showcase">
      <div class="study-showcase-image">
        <img class="img img-responsive" src="./images/temurin-case-study.webp">
      </div>
      <div class="study-showcase-content">
        <p class="study-showcase-pretitle">Case Study</p>
        <h2 class="study-showcase-title">Eclipse Temurin: Pioneering Software Supply Chain Security</h2>
        <p>
          Find out how the Eclipse Foundation and Adoptium Working Group are
          working to build the world's most secure OpenJDK distribution
        </p>
        <a class="btn btn-primary btn-sm btn-block" href="https://outreach.eclipse.foundation/adoptium-temurin-supply-chain-security">
          Download Now
        </a>
      </aside>
    </div>
  </div>
hide_page_title: true
hide_sidebar: true
jumbotron_class: "col-xs-24"
custom_jumbotron_class: "col-xs-24" 
container: "container-fluid adoptium-summit-2024"
layout: "single"
cascade:
  logo: "/events/2024/adoptium-summit/images/adoptium-summit.png"
  logo_width: 165 
  header_wrapper_class: "adoptium-summit-2024"
  page_css_file: "/events/css/2024-adoptium-summit.css"
---

{{< 2024/adoptium_summit/registration >}}

<!-- Speakers -->
{{< grid/section-container class="featured-section padding-y-60 eclipsefdn-user-display-circle" >}}
  {{< events/user_display title="Meet our Speakers" event="adoptium-summit" year="2024"  source="speakers" imageRoot="./images/speakers/" subpage="speakers" displayLearnMore="true" >}}
  {{</ events/user_display >}}
{{</ grid/section-container >}}

<!-- Agenda -->
{{< grid/section-container class="featured-section padding-y-60" >}}
  {{< events/agenda event="adoptium-summit" year="2024" >}}
  {{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
{{</ grid/section-container >}}

<!-- Committee -->
{{< grid/section-container class="featured-section padding-y-60 eclipsefdn-user-display-circle" >}}
  {{< events/user_display event="adoptium-summit" year="2024"  source="committee" imageRoot="./images/committee/" subpage="program-committee" displayLearnMore="true" >}}
  {{</ events/user_display >}}
{{</ grid/section-container >}}
