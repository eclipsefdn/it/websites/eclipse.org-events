---
title: "eSAAM 2024 on Data Spaces"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2024 on Data Spaces</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        <strong>4th Eclipse Security, AI, Architecture and Modelling Conference<br/>on Data Spaces</strong>
    </h2>
custom_jumbotron_class: container-fluid
date: 2024-10-22T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
show_featured_footer: false
hide_call_for_action: true
header_wrapper_class: "header-esaam2024-event"
hide_breadcrumb: true
container: "container-fluid esaam-2024-event"
summary: "For its fourth edition, eSAAM 2024 was about data spaces. Come together with industry experts and researchers working on innovative software and systems solutions for data spaces, specifically focusing on security and privacy, artificial intelligence and machine learning, systems and software architecture, modelling and related challenges."
layout: single
main_menu: esaam2024
keywords: ["eclipse", "UOM", "CERTH", "ITI", "conference", "research", "eSAAM", "Dataspace", "Data Space", "Security", "AI", "Architecture", "Modelling", "Modeling"]
links: [ [href: "#agenda", text: "Agenda & Proceedings"]  ]
---

[//]: # (Introduction)
{{< grid/section-container id="about" class="featured-section-row featured-section-row-light-bg text-center" isMarkdown="false">}}
	<h2>The 4th Eclipse SAAM on Data Space 2024 is now over!<br/>Thank you for your interest and attendance!</h2> 
	<p align="justify">
		The conference brought together industry experts and researchers working on innovative software and systems solutions for data spaces, 
		specifically focusing on security and privacy, artificial intelligence and machine learning, systems and software architecture, modelling and related challenges.
	</p>
	<p align="justify">
		The importance of data as a valuable asset for companies, organisations, and public administrations has continuously increased over the past decade. According to the EU, 
		it is reshaping how we produce, consume, and live. Data sharing in real-world ecosystems has introduced new value opportunities for the entities that exchange data. 
		However, areas such as data exchange and monetization are still in their early stages of development.
	</p>
	<p align="justify">
		In recent years, the concept of data spaces is continuously gaining interest on a global level and a series of developments and challenges regarding them have been 
		highlighted. Data spaces are anticipated to advance the state of the art in data exchange and monetization by providing a secure and trusted environment where 
		organisations can share and access data. Data spaces will foster collaboration and enhance the discoverability, accessibility, and reusability of data while 
		ensuring interoperability.
	</p>
	<p align="justify">
		However, this endeavour presents various challenges, and in particular, these challenges are related both to data interoperability and ICT infrastructure interoperability.
		Moreover, alongside governance, concepts of data sovereignty and trust have been key challenges related to data sharing.
	</p>
	<p align="justify">
		A plethora of emerging architectures, concepts, technologies, methodologies, associations, and initiatives are currently under investigation and development to establish
		these ecosystems based on common principles and standards. These include the development of data connectors, privacy preservation techniques, vocabularies, 
		metadata catalogues, and data sovereignty frameworks. 
	</p>
	<p align="justify">
		Researchers and professionals engaged in these fields were encouraged to present their work to the community, comprising industry stakeholders, standardisation bodies, and
		open-source initiatives at eSAAM 2024 on Data Spaces.
	</p>
	<p align="center">
		The event was co-located with <br/>
		<a href="https://www.ocxconf.org/event/2024/summary"><img src="images/ocx2024.png" width="200"/></a>
	</p>
{{</ grid/section-container >}}


[//]: # (Topics)
{{< grid/section-container id="topics" class="featured-section-row featured-section-row-light-bg text-center">}}
	<h2>Technical topics of interest in Data Spaces</h2>
	{{< grid/div class="row" isMarkdown="false">}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Security and Privacy for the Cloud to Edge Continuum](images/security-black.png)](topics/index.html#security-and-privacy-for-the-cloud-to-edge-continuum)
### [Security and Privacy](topics/index.html#security-and-privacy-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Artificial Intelligence for the Cloud to Edge Continuum](images/ai-black.png)](topics/index.html#artificial-intelligence-for-the-cloud-to-edge-continuum)
### [Artificial Intelligence](topics/index.html#artificial-intelligence-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Architectures for the Cloud to Edge Continuum](images/icon-architecture.png)](topics/index.html#architectures-for-the-cloud-to-edge-continuum)
### [Architecture](topics/index.html#architectures-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Modelling for the Cloud to Edge Continuum](images/modeling-black.png)](topics/index.html#modelling-for-the-cloud-to-edge-continuum)
### [Models and Services](topics/index.html#modelling-for-the-cloud-to-edge-continuum)
		{{</ grid/div >}}
	{{</ grid/div >}}
{{</ grid/section-container >}}

[//]: # (Speakers)
{{< grid/section-container id="speakers" class="featured-section-row text-center featured-section-row-highligthed-bg eclipsefdn-user-display-circle" >}}
  {{< events/esaam_user_display event="esaam2024" year="2024" title="Speakers" 
  		source="speakers" imageRoot="/events/2024/esaam2024/images/speakers/" subpage="speakers" displayLearnMore="false" />}}
{{</ grid/section-container >}}

[//]: # (Agenda)
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-light-bg" title="Agenda">}}
  {{< events/esaam_agenda event="esaam2024" year="2024" >}}
{{</ grid/section-container >}}

[//]: # (Proceedings)
{{< grid/section-container id="proceedings" class="featured-section-row featured-section-row-highligthed-bg text-center">}}
	<h2>Proceedings</h2>
	<a href="https://dl.acm.org/doi/proceedings/10.1145/3685651"><img src="images/ACM_ICPS_v.2B.png" width="200"/></a><br/>
	<p>eSAAM 2024 proceedings are now on the 
	<a href="https://dl.acm.org/doi/proceedings/10.1145/3685651"><strong>ACM Digital Library</strong></a></p>
{{</ grid/section-container >}}

[//]: # (Indexed by)
{{< grid/section-container id="indexes" class="featured-section-row featured-section-row-highligthed-bg text-center">}}
    <h2>Indexed by</h2>
	{{< grid/div class="row" isMarkdown="false">}}
	<!--
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![Scopus](images/scopus_logo.png)](https://www.scopus.com/)
		{{</ grid/div >}}
	-->
		{{< grid/div class="col-md-8 padding-bottom-20" isMarkdown="true">}}
[![Google Scholar](images/GScholar_logo.png)](https://scholar.google.com/scholar?as_ylo=2024&q=esaam+2024)
		{{</ grid/div >}}
		{{< grid/div class="col-md-8 padding-bottom-20" isMarkdown="true">}}
[![dblp](images/dblp_logo.png)](https://dblp.org/search?q=%22esaam%202024%22)
		{{</ grid/div >}}
		{{< grid/div class="col-md-8 padding-bottom-20" isMarkdown="true">}}
[![Semantic Scholar](images/semantic_scholar_logo.png)](https://www.semanticscholar.org/search?q=%22esaam%202024%22)
		{{</ grid/div >}}
	<!--
		{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="true">}}
[![ORCID](images/ORCID-logo.png)](https://orcid.org/)
		{{</ grid/div >}}
	{{</ grid/div >}}
	-->
{{</ grid/section-container >}}


[//]: # (Best Paper Award)
{{< grid/section-container id="proceedings" class="featured-section-row featured-section-row-light-bg text-center">}}
	<h2>Best Paper Award</h2>
	<p>We are delighted to announce that the Technical Program Committee has awarded <br/>the eSAAM 2024 Best Paper Award to:<br/>
	<!--
	<strong>Amir Shayan Ahmadian, Sebastian Franke, Charly Gnoguem, and Jan Juerjens</strong> 
	<br/>for their paper on <br/><i><strong>Privacy-Friendly Sharing of Health Data Using a Reference Architecture for Health Data Spaces</strong></i>.
	-->
	</p>
	<a href="https://dl.acm.org/doi/10.1145/3685651.3685657">
		<img src="images/eSAAM2024-Best Paper Award.png" width="800" alt="Best Paper Award"/>
	</a>
	
{{</ grid/section-container >}}

[//]: # (TCP)
<!-- PROGRAM COMMITTEE --> 
{{< grid/section-container id="program-committee" class="featured-section-row featured-section-row-highligthed-bg">}}
<h2>Technical Program Committee</h2>

<p>The Technical Program Committee is an independent panel of expert volunteers and as such will do their best to judge papers objectively and on the principle of a level playing field for all. </p>

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Alessandra Bagnato, Softeam Group
* Alessio Sacco, POLITO
* Ana Isabel Torre Bastida, Tecnalia
* Anastasios Zafeiropoulos, NTUA
* Apostolos Ampatzoglou, U. of Macedonia
* Athanasios Naskos, Atlantis Engineering
* Daniel Alonso, BDVA
* Davide Taibi, Tampere University
* Ella Peltonen, U. of Oulu
* Fulvio Risso, POLITO
{{</ grid/div >}}

{{< grid/div class="col-md-12 padding-bottom-20" isMarkdown="true">}}
* Fulya Horozal, ATB-Bremen
* Ilknur Chulani, IDSA
* Javier Serrano, UPM
* Joaquín Salvachúa, UPM
* Marcin Plociennik, PSNC
* Panagiotis Papadimitriou, U.of Macedonia
* Peter Bednar, TUKE
* Raúl Palma, PSNC
* Teodoro Montanaro, U. del Salento
* Usman Wajid, ICE
{{</ grid/div >}}
{{</ grid/div >}}

{{</ grid/section-container >}}

[//]: # (Organizing Committee)
{{< grid/section-container id="committee" class="featured-section-row text-center featured-section-row-light-bg eclipsefdn-user-display-circle" >}}
<h2>Organizing Committee</h2>
{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="false">}}
<a href="https://www.uom.gr/en"><img src="images/organizers/logo-uom.png" width="200" alt="University of Macedonia"/></a>
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="false">}}
<a href="https://eclipse.org"><img src="images/organizers/logo-certh.png" width="200" alt="CERTH"/></a>
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="false">}}
<a href="https://eclipse.org"><img src="images/organizers/logo-iti.png" width="200" alt="ITI"/></a>
{{</ grid/div >}}

{{< grid/div class="col-md-6 padding-bottom-20" isMarkdown="false">}}
<a href="https://eclipse.org"><img src="images/organizers/logo-ecl.png" width="200" alt="Eclipse Foundation"/></a>
{{</ grid/div >}}

{{</ grid/div >}}

{{< grid/div class="row" isMarkdown="false">}}

{{< grid/div class="col-md-12 padding-bottom-20 text-left" isMarkdown="true">}}
## Conference Co-Chairs
* **Jordi Arjona Aroca**, Instituto Tecnológico de Informática
* **Panagiotis Papadimitriou**, University of Macedonia
* **Alexandros Nizamis**, Centre for Research & Technology, Hellas
* **Philippe Krief**, Eclipse Foundation
{{</ grid/div >}}

{{< grid/div class="col-md-12 padding-bottom-20 text-left" isMarkdown="true">}}
## Program Committee Chairs
* **Liliana Beltrán Blanco**, Instituto Tecnológico de Informática
* **Panagiotis Papadimitriou**, University of Macedonia
* **Alexandros Nizamis**, Centre for Research & Technology, Hellas
* **Georgios Spanos**, Centre for Research & Technology, Hellas
* **Rosaria Rossini**, Eclipse Foundation
{{</ grid/div >}}
{{</ grid/div >}}
<!--
	{{< events/esaam_user_display event="esaam2024" year="2024" title="Organizing Committee" source="committee" imageRoot="/events/2024/esaam2024/images/speakers/" subpage="committee" displayLearnMore="false" />}}
-->
{{</ grid/section-container >}}

[//]: # (Sponsors)
{{< grid/section-container id="sponsors" class="featured-section-row featured-section-row-highligthed-bg text-center">}}
  {{< events/sponsors event="esaam2024" year="2024" source="sponsors" title="Our Sponsors" useMax="false" displayBecomeSponsor="false">}}
{{</ grid/section-container >}}

[//]: # (Previous Conferences)
{{< grid/section-container id="previous-esaam" class="featured-section-row featured-section-row-light-bg" isMarkdown="true">}}
## Previous conferences
* [2023 - eSAAM on Cloud-to-Edge Continuum](https://events.eclipse.org/2023/esaam2023/)
* [2021 - eSAAM on Mobility](https://events.eclipse.org/2021/saam-mobility/)
* [2020 - eSAM IoT](https://events.eclipse.org/2020/sam-iot/)

{{</ grid/section-container >}}

{{< grid/section-container id="resources" class="featured-section-row featured-section-row-highligthed-bg" isMarkdown="true">}}
## Images from
* [Header by Roman from Pixabay](https://pixabay.com/illustrations/cloud-computer-circuit-board-cpu-6532831/)
* [Security and Privacy icon by F.Adiima from NounProject](https://thenounproject.com/search/?q=security&i=3349833)
* [Artificial Intelligence icon by priyanka from NounProject](https://thenounproject.com/search/?q=artificial%20intelligence&i=2858867)
* [Architecture icon by SBTS from NounProject](https://thenounproject.com/icon/web-architecture-1711291)
* [Models and Services icon by G.Tachfin from NounProject](https://thenounproject.com/search/?q=modelling&i=2710243)
{{</ grid/section-container >}}


{{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
