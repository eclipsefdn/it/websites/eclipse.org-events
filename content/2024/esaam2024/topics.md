---
title: "eSAAM 2024 Topics"
headline: ''
custom_jumbotron: |
    <h1 class="featured-jumbotron-title text-center">eSAAM 2024 Topics</h1>
    <h2 class="featured-jumbotron-subtitle text-center">
        3rd Eclipse Security, AI, Architecture and Modelling Conference<br/>on Data Spaces
    </h2>
    <h3 class="featured-jumbotron-subtitle text-center" style="color: white;">October 22, 2024 | Mainz, Germany</h3>
custom_jumbotron_class: container-fluid
date: 2024-10-22T08:00:00-04:00
hide_page_title: true
hide_sidebar: true
header_wrapper_class: "header-esaam2024-event"
hide_breadcrumb: true
show_featured_footer: false
hide_call_for_action: true
main_menu: esaam2023  
container: "container-fluid esaam-2024-event"
summary: "For its fourth edition, eSAAM 2024 was about data spaces. Come together with industry experts and researchers working on innovative software and systems solutions for data spaces, specifically focusing on security and privacy, artificial intelligence and machine learning, systems and software architecture, modelling and related challenges."
layout: single
keywords: ["eclipse", "UOM", "CERTH", "ITI", "conference", "research", "eSAAM", "Dataspace", "Data Space", "Security", "AI", "Architecture", "Modelling", "Modeling"]
links: [ [href: "..", text: "Main Page"] ]
draft: false
---

{{< grid/section-container id="topics" class="featured-section-row">}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Data Space Security and Privacy
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Data Space Security and Privacy](../images/security-black.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Data Encryption Techniques
* Access Control and Authorization
* Data Anonymization and Pseudonymization
* Secure Data Transmission
* Data Masking and Obfuscation
* Data Governance and Compliance
* Privacy-Preserving Data techniques
* Data Privacy Impact Assessments (DPIA)
* Data Lifecycle Management
* Data Auditing and Monitoring
* Collaborative Data Space Security
* Sovereign Data Connectors and Applications

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### AI, and Machine Learning in Data Space
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![AI, and Machine Learning in Data Space](../images/ai-black.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Federated Learning, Split Learning & Transfer Learning for Connected Data Ecosystems
* Explainable AI (XAI)
* AI and Privacy, Ethics & Bias
* Machine Learning Ops (MLOps)
* AI for Data Governance
* Edge AI and Data Spaces
* AI Applications for Data Sharing Ecosystems

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Data Space Architecture
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Data Space Architecture](../images/icon-architecture.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Data Space Architecture Fundamentals
* Data Modeling and Design
* Data Integration Architectures
* Real-time Data Processing
* Metadata Management and Data Spaces Brokers
* Big Data Architectures
* Data Security  Architectures
* Data Sharing Architecture for AI and Machine Learning
* Cloud-edge Architecture for Data Spaces
* Connected and Federated Data Spaces Architectures
* Evaluation Framework for Data Spaces and Best Practices 

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Modelling for Data Space Systems
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-light-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
![Modelling for Data Space Systems](../images/modeling-black.png)
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* Conceptual Modelling
* Data Governance Models
* Vocabularies and Ontologies for Data Sharing Ecosystems
* Data and Infrastructure Interoperability
* Best Practices for Interoperable Data Exchange 
* Data Quality
* Data Compliance

{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-24 padding-bottom-20 text-center" isMarkdown="true">}}
### Images from
{{</ grid/div >}}
{{</ grid/div >}}

{{< grid/div class="row featured-section-row-highligthed-bg" isMarkdown="false">}}
{{< grid/div class="col-md-6 padding-bottom-20 text-center" isMarkdown="true">}}
{{</ grid/div >}}

{{< grid/div class="col-md-18 padding-bottom-20" isMarkdown="true">}}
* [Header by Roman from Pixabay](https://pixabay.com/illustrations/cloud-computer-circuit-board-cpu-6532831/)
* [Security and Privacy icon by F.Adiima from NounProject](https://thenounproject.com/search/?q=security&i=3349833)
* [Artificial Intelligence icon by priyanka from NounProject](https://thenounproject.com/search/?q=artificial%20intelligence&i=2858867)
* [Architecture icon by SBTS from NounProject](https://thenounproject.com/icon/web-architecture-1711291)
* [Models and Services icon by G.Tachfin from NounProject](https://thenounproject.com/search/?q=modelling&i=2710243)

{{</ grid/div >}}
{{</ grid/div >}}

{{</ grid/section-container >}}