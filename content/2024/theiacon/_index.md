---
title: "TheiaCon 2024"
seo_title: "TheiaCon 2024"
date: 2024-08-06
logo: "/events/2023/theiacon/images/logos/theiacon-logo.svg"
headline: ""
header_wrapper_class: "header-theiacon-event header-theiacon-event-2022"
custom_jumbotron: |
  <div class="theiacon-jumbotron-2023 padding-bottom-20">
    <div class="jumbotron-head">
      <div class="jumbotron-title-container col-md-14">
        <h1 class="jumbotron-title">TheiaCon 2024</h1>
        <p class="jumbotron-subtitle">Leading the Next Generation of IDE and Tool Development</p>
        <p class="jumbotron-details-text"><span class="uppercase">Virtual Event</span> | 13 - 14 November, 2024</p>
      </div>
      <a class="col-xs-18 col-sm-12 col-md-10" href="https://outreach.eclipse.foundation/theia-ide-developer-tools">
        <div class="jumbotron-graphic-container gradient-backed-content-wrapper split-content">
          <img class="gradient-backed-content" src="/events/2023/theiacon/images/theia-brief-tablet.webp" alt="" />
          <div>
            <div class="bottom-aligned-text">
              <p class="small">Learn more about Eclipse Theia</p>
              <p class="padding-left-30">
                <span class="brand-primary fw-700 uppercase">Project brief:</span><br />
                Why Eclipse Theia is ideal to Build Modern IDEs and Developer
                Tools
              </p>
            </div>
          </div>
        </div>
      </a>
    </div>
  </div>
custom_jumbotron_class: "container-fluid"
hide_page_title: true
hide_sidebar: true
container: "container-fluid"
layout: "single"
---

<!-- About Us -->
{{< grid/section-container id="registration" class="featured-section-row featured-section-row-lighter-bg padding-y-60">}}
  {{< events/registration event="theiacon" year="2024" >}}
  {{</ events/registration >}}
{{</ grid/section-container >}}

<!-- Speakers -->
{{< grid/section-container class="featured-section-row featured-section-row-dark-bg" >}}
  {{< grid/div id="speakers" class="row padding-y-60 eclipsefdn-user-display-circle text-center" isMarkdown="false" >}}
    {{< events/user_display title="Speakers" event="theiacon" year="2024" source="speakers" headerClass="fw-500" imageRoot="/events/2024/theiacon/images/speakers/" subpage="./speakers" displayLearnMore="false" >}}
    {{</ events/user_display >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Program Committee -->
{{< grid/section-container >}}
  {{< grid/div id="committee" class="row padding-y-60 eclipsefdn-user-display-circle text-center" isMarkdown="false" >}}
    {{< events/user_display event="theiacon" year="2024" source="committee" headerClass="fw-500" imageRoot="/events/2024/theiacon/images/committee/" subpage="./program-committee" displayLearnMore="false" >}}
    {{</ events/user_display >}}
  {{</ grid/div >}}
{{</ grid/section-container >}}

<!-- Agenda -->
{{< grid/section-container id="agenda" class="featured-section-row featured-section-row-lighter-bg" isMarkdown="false" >}}
  {{< events/agenda event="theiacon" year="2024" >}}
  {{< bootstrap/modal id="eclipsefdn-modal-event-session" >}}
{{</ grid/section-container >}}

<!-- Sponsors -->
{{< theiacon/sponsored-section class="text-center padding-y-60 featured-section-row featured-section-row-lighter-bg" working_group="cloud-development-tools" display_sponsors="true" year="2024" event="theiacon" >}}
